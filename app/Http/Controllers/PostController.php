<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        
        return view('post.index', ['posts' => $posts]);
    }
    
    public function show($id)
    {
        $post = Post::findOrFail($id);
        
        return view('post.show', ['post' => $post]);
    }
    
    public function create(Request $request)
    {
        // 回傳View/post/create.blade.php
        return view('post.create');
    }
    
    public function store(Request $request)
    {
        // 把$request丟過來的包裹儲存成一個Model包裹
        Post::create([
            'title'=>$request->input('title'),
            'content'=>$request->input('content')
        ]);
        
        // 重新導向
        return redirect()->route('post.index');
    }
    
    public function destroy($id)
    {
        // 先去資料庫找找看這個post存不存在
        $post = Post::findOrFail($id);
        // 找到後呼叫刪除函式
        $post->delete();
        
        // 重新導向
        return redirect()->route('post.index');
    }
}
